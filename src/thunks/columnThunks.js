import { createAsyncThunk } from '@reduxjs/toolkit';
import { addColumn, getDataColumn } from '../features/columnSlice';
import { hideModalColumn } from '../features/modalColumnSlice';

export const addColumnAndHideModal = createAsyncThunk(
  'column/addColumnAndHideModal',
  async (columnData, { dispatch, getState }) => {

    // Dispatch l'action addColumn
    await dispatch(addColumn(columnData));

    // Récupérer l'état après l'action
    let lastAddColumnSuccess = await getDataColumn(getState());
    lastAddColumnSuccess = lastAddColumnSuccess.payload.column.addSuccessColumn;

    // Vérifier le succès et conditionnellement dispatch hide
    if (lastAddColumnSuccess) {
      dispatch(hideModalColumn());
    }
  }
);