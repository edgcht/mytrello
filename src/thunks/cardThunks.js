import { createAsyncThunk } from '@reduxjs/toolkit';
import { hideModalCard } from '../features/modalCardSlice';
import { addCard, updateCard, getDataCard } from '../features/cardSlice';

export const addCardandHideModal = createAsyncThunk(
  'column/addCardandHideModal',
    async (cardData, { dispatch, getState }) => {

    await dispatch(addCard(cardData));

    let lastAddCardSuccess = await getDataCard(getState());
    lastAddCardSuccess = lastAddCardSuccess.payload.card.submissionCardIsValid;

    if(lastAddCardSuccess.name && lastAddCardSuccess.type){
        dispatch(hideModalCard())
    }
  }
);

export const updateCardAndHideModal = createAsyncThunk(
  'column/updateCardAndHideModal',
    async (cardData, { dispatch, getState }) => {

    await dispatch(updateCard(cardData));

    let lastAddCardSuccess = await getDataCard(getState());
    lastAddCardSuccess = lastAddCardSuccess.payload.card.submissionCardIsValid;

    if(lastAddCardSuccess.name && lastAddCardSuccess.type){
      dispatch(hideModalCard())
    }
  }
);