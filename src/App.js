
import './App.css';
import Row from './components/Row';
import AddColumn from './components/AddColumn';
import ModalColumn from './components/ModalColumn';
import ModalCard from './components/ModalCard'
import { useSelector } from 'react-redux';

function App() {

  const columnValue = useSelector((state) => state.column.columnValue);

  return (
    <div className='bg-gray-100 min-h-screen min-w-max'>
      <header className='w-full h-14 bg-white flex items-center justify-center'>
        <h1 className='text-xl fixed left-5'>My Trello</h1>
      </header>
      <div className="flex gap-4 p-4 w-full items-start">

        {
          columnValue.map((columnValue, index) => {
            return (
              <Row key={index} title={columnValue.name}/>
            )
          })
        }

        <AddColumn/>
        <ModalColumn/>
        <ModalCard/>

      </div>
    </div>
  );
}

export default App;
