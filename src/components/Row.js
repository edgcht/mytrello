import { useState, useEffect } from 'react';
import { TrashIcon, DocumentPlusIcon } from '@heroicons/react/24/solid';
import { deleteColumn } from '../features/columnSlice';
import { showModalCard } from '../features/modalCardSlice';
import { setColumn, setCurrentItem, deleteCardByColumn } from '../features/cardSlice';
import { useDispatch, useSelector } from 'react-redux';
import Card from './Card';

const Row = ({title}) => {

  const typeAction = 'create';
  const cardValue = useSelector((state) => state.card.cardValue);
  const [data, setData] = useState(cardValue);

  const dispatch = useDispatch();

  useEffect(() => {
    setData(
      cardValue.filter((element) => 
        element.column === title
      )
    )
  }, [cardValue, title])

  return (
    <div className="Row w-64 min-w-[256px] bg-gray-200 rounded-xl p-2 ">
        <div className="w-full flex p-4 bg-white rounded-xl justify-between">
            <h1 className="">{title}</h1>
            <div className='flex'>
              <DocumentPlusIcon className="h-6 w-6 text-slate-300 hover:cursor-pointer relative right-1" 
                onClick={() => {
                  dispatch(showModalCard(typeAction)); 
                  dispatch(setColumn(title)); 
                  dispatch(setCurrentItem(null))} 
                }
              />
              <TrashIcon className="h-6 w-6 text-red-800 hover:cursor-pointer" 
                onClick={() => {
                  dispatch(deleteColumn(title))
                  dispatch(deleteCardByColumn(title))}
                }
              />
            </div>
        </div>

        {
          data.map((item, index) => {
            return (<Card key={index} dataCard={item}/>)
          })
        }

    </div>
  );
}

export default Row;