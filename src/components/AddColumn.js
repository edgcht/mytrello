import { PlusIcon } from '@heroicons/react/24/solid';
import { showModalColumn } from '../features/modalColumnSlice';
import { useDispatch } from 'react-redux';

const AddColumn = () => {

  const dispatch = useDispatch();

  return (
    <div className="AddColumn Row w-16 bg-gray-200 rounded-xl p-2 hover:cursor-pointer">
        <div className="p-4 bg-white rounded-xl">
            <PlusIcon className="h-6 w-6 text-blue-500 relative right-1" onClick={() => dispatch(showModalColumn())}/>
        </div>
    </div>
  );
}

export default AddColumn;
