const InfoError = ({message}) => {
    return (
      <div className="absolute z-50 bottom-3 right-3 p-14 bg-orange-400 animate-bounce">
          <div className="w-full fle rounded-xl justify-between h-full text-white text-xl">
              <h1 className="">{message}</h1>
          </div>
      </div>
    );
  }
  
  export default InfoError;
  