import { Fragment, useRef } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { hideModalCard } from '../features/modalCardSlice';
import { useSelector, useDispatch } from 'react-redux';
import { deleteCard, clearErrorCard } from '../features/cardSlice';
import { addCardandHideModal, updateCardAndHideModal } from '../thunks/cardThunks';
import generateGUID from '../utils/functions'

const ModalCard = () => {

    const columnValue = useSelector((state) => state.column.columnValue);
    const modalCard = useSelector((state) => state.modalCard.modalCardValue);
    const currentItem = useSelector((state) => state.card.currentItem);
    const modalActionType = useSelector((state) => state.modalCard.actionType);
    const cardAddToColumn = useSelector((state) => state.card.column);
    const submissionCardIsValid = useSelector((state) => state.card.submissionCardIsValid);

    const dispatch = useDispatch();

    const inputCardNameRef = useRef();
    const inputCardColonneRef = useRef();
    const inputCardTagRef = useRef();
    const inputCardDescRef = useRef();
    const cancelButtonRef = useRef(null);

    let dataCard;

    if(currentItem){
      dataCard = {
        titleModal: 'modifier le carte',
        id: currentItem.id,
        column: currentItem.column,
        description: currentItem.data.description,
        name: currentItem.data.name,
        type: currentItem.data.type
      }
    }else{
      dataCard = {
        titleModal: 'Créer une carte',
        description: null,
        name: null,
        type: null
      }
    }

    return (
        <Transition.Root show={modalCard} as={Fragment}>
          <Dialog as="div" 
            className="relative z-10" 
            initialFocus={cancelButtonRef} 
            onClose={() => {
              dispatch(hideModalCard());
              dispatch(clearErrorCard())
            }}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>
    
            <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
              <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                >
                  <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                    <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                      <div className="sm:flex sm:items-start">
                        <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left w-full">
                          <form>
                            <div className="space-y-12">

                              <div className=" border-gray-900/10 pb-6">
                                <h2 className="text-base font-semibold leading-7 text-gray-900">{dataCard.titleModal}</h2>
                                <div className="mt-6 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                  {
                                    currentItem && (
                                      <div className="col-span-full w-full">
                                        <label htmlFor="country" className="block text-sm font-medium leading-6 text-gray-900">
                                          Colonne
                                        </label>
                                        <div className="mt-2 w-full">
                                          <select
                                            ref={inputCardColonneRef}
                                            id="country"
                                            name="country"
                                            defaultValue={dataCard.column}
                                            autoComplete="country-name"
                                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                          >
                                            {
                                              columnValue.map((item, index) => {
                                                return(
                                                  <option value={item.name} key={index}>{item.name}</option>
                                                )
                                              })
                                            }
                                          </select>
                                        </div>
                                      </div>
                                    )
                                  }
                                  <div className="col-span-full">
                                    <label htmlFor="about" className="block text-sm font-medium leading-6 text-gray-900">
                                      Nom de la carte
                                    </label>
                                    <div className="mt-2">
                                      <input
                                          ref={inputCardNameRef}
                                          defaultValue={dataCard.name}
                                          type="text"
                                          name="username"
                                          id="username"
                                          autoComplete="username"
                                          className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                          placeholder="janesmith"
                                      />
                                      { !submissionCardIsValid.name && <p class="text-red-500 text-xs italic">Veuillez saisir un nom</p>}
                                      
                                    </div>
                                  </div>
                                  <div className="col-span-full w-full">
                                    <label htmlFor="country" className="block text-sm font-medium leading-6 text-gray-900">
                                      Tag
                                    </label>
                                    <div className="mt-2 w-full">
                                      <select
                                        ref={inputCardTagRef}
                                        id="country"
                                        name="country"
                                        defaultValue={dataCard.type}
                                        autoComplete="country-name"
                                        className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                      >
                                        <option value="">-- Choisissez un tag--</option>
                                        <option value={'Bas'}>Basse priorité</option>
                                        <option value={'Moyen'}>Moyenne priorité</option>
                                        <option value={'Haut'}>Haute priorité</option>
                                        <option value={'Tres haut'}>Très Haut priorité</option>
                                        <option value={'Urgent'}>Priorité Urgent</option>
                                      </select>
                                      { !submissionCardIsValid.type && <p class="text-red-500 text-xs italic">Veuillez sélectionnez un tag</p>}
                                    </div>
                                  </div>
                                  <div className="col-span-full w-full">
                                    <label htmlFor="country" className="block text-sm font-medium leading-6 text-gray-900">
                                      Description
                                    </label>
                                    <div className="mt-2 w-full">
                                      <textarea
                                        ref={inputCardDescRef}
                                        id="about"
                                        name="about"
                                        rows={3}
                                        className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        defaultValue={dataCard.description}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                            <div className="mt-6 flex items-center justify-between gap-x-6">
                                {currentItem && (
                                    <span
                                      type="button"
                                      className="mt-3 inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm ring-inset ring-gray-300 hover:bg-red-400 hover:cursor-pointer sm:mt-0 sm:w-auto"
                                      onClick={() => {
                                          
                                        dispatch(hideModalCard()); dispatch(deleteCard(
                                        {
                                          id: dataCard.id,
                                          column: inputCardColonneRef.current.value, data: {
                                          name: inputCardNameRef.current.value,
                                          type: inputCardTagRef.current.value,
                                          description: inputCardDescRef.current.value
                                        }}
                                      ))}}
                                      ref={cancelButtonRef}
                                    >
                                      Supprimer
                                    </span>
                                  )
                                }
                                <div className='flex gap-2'>
                                  <span
                                    onClick={(e) => {
                                        if(modalActionType === 'update'){
                                          dispatch(updateCardAndHideModal([
                                            {
                                              id: dataCard.id,
                                              column: inputCardColonneRef.current.value, data: {
                                              name: inputCardNameRef.current.value,
                                              type: inputCardTagRef.current.value,
                                              description: inputCardDescRef.current.value
                                            }},
                                            {
                                              id: dataCard.id,
                                              column: dataCard.column, data: {
                                              name: dataCard.name,
                                              type: dataCard.type,
                                              description: dataCard.description
                                            }}
                                          ]))
                                        }else{
                                          dispatch(addCardandHideModal({
                                              id: generateGUID(),
                                              column: cardAddToColumn, data: {
                                              name: inputCardNameRef.current.value,
                                              type: inputCardTagRef.current.value,
                                              description: inputCardDescRef.current.value
                                            }
                                          }))
                                        }
                                      }
                                    }
                                    type="submit"
                                    className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 hover:cursor-pointer focus-visible:outline-indigo-600"
                                  >
                                    Enregistrer
                                  </span>
                                  <span
                                        type="button"
                                        className="mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:cursor-pointer hover:bg-gray-50 sm:mt-0 sm:w-auto"
                                        onClick={() => {
                                          dispatch(hideModalCard()); 
                                          dispatch(clearErrorCard())
                                        }}
                                        ref={cancelButtonRef}
                                    >
                                        Annuler
                                  </span>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition.Root>
      )
}

export default ModalCard;
