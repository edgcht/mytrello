import React, { useState, useEffect } from 'react';

const Tag = ({type}) => {

  const [color, setColor] = useState();

  useEffect(() => {
    switch (type) {
      case 'Bas':
        setColor('bg-green-500');
        break;
      case 'Moyen':
        setColor('bg-orange-300');
        break;
      case 'Haut':
        setColor('bg-orange-500');
        break;
      case 'Tres haut':
        setColor('bg-red-600');
        break;
      case 'Urgent':
        setColor('bg-purple-800');
        break;
      default:
        setColor('bg-lime-400');
    }
  }, [type]); 

  return (
    <div className={`Tag h-2 w-9 rounded-lg ${color}`}></div>
  );
}

export default Tag;
