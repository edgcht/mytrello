import React, { useState } from 'react';

const DragDrop = () => {
  const [draggedItem, setDraggedItem] = useState(null);

  const handleDragStart = (e, item) => {
    setDraggedItem(item);
    e.dataTransfer.setData("text", item);
    e.dataTransfer.effectAllowed = "move";
  };

  const handleDragOver = (e) => {
    e.preventDefault();
    e.dataTransfer.dropEffect = "move";
  };

  const handleDrop = (e, dropZone) => {
    e.preventDefault();
    const item = e.dataTransfer.getData("text");
    // Ici, vous pouvez mettre à jour l'état pour refléter le déplacement
  };

  return (
    <div>
      <div
        draggable
        onDragStart={(e) => handleDragStart(e, 'Élément 1')}
        style={{ width: '100px', height: '100px', backgroundColor: 'skyblue', margin: '10px' }}
      >
        Élément 1
      </div>
      <div
        onDragOver={handleDragOver}
        onDrop={(e) => handleDrop(e, 'Zone de dépôt')}
        style={{ width: '300px', height: '300px', backgroundColor: 'lightgrey', margin: '10px' }}
      >
        Zone de dépôt
      </div>
    </div>
  );
};

export default DragDrop;
