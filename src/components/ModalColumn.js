import { Fragment, useRef } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { hideModalColumn } from '../features/modalColumnSlice';
import { useSelector, useDispatch } from 'react-redux';
import { setErrorName, clearErrorColumn } from '../features/columnSlice';
import { addColumnAndHideModal } from '../thunks/columnThunks';


const ModalColumn = () => {

    const modalColumnValue = useSelector((state) => state.modalColumn.modalColumnValue);
    const errorColumnIsExist = useSelector((state) => state.column.errorColumnIsExist);
    const errorNameIsExist = useSelector((state) => state.column.errorNameIsExist);
    const dispatch = useDispatch();

    const inputColumnNameRef = useRef();
    const cancelButtonRef = useRef(null);

    return (
        <Transition.Root show={modalColumnValue} as={Fragment}>
          <Dialog as="div" className="relative z-10" initialFocus={cancelButtonRef} 
            onClose={() => {
              dispatch(hideModalColumn());
              dispatch(clearErrorColumn())
            }}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
            </Transition.Child>
    
            <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
              <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  enterTo="opacity-100 translate-y-0 sm:scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                  leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                >
                  <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                    <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                      <div className="sm:flex sm:items-start">
                        <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left w-full">
                          <form>
                            <div className="space-y-12">

                              <div className=" border-gray-900/10 pb-6">
                                <h2 className="text-base font-semibold leading-7 text-gray-900">Ajouter une colonne</h2>
                                <div className="mt-6 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                <div className="col-span-full">
                                    <label htmlFor="about" className="block text-sm font-medium leading-6 text-gray-900">
                                      Nom de la colonne
                                    </label>
                                    <div className="mt-2">
                                      <input
                                          required
                                          ref={inputColumnNameRef}
                                          type="text"
                                          name="username"
                                          id="username"
                                          autoComplete="username"
                                          className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                          placeholder="janesmith"
                                      />
                                      { errorColumnIsExist && <p class="text-red-500 text-xs italic">Le nom de la colonne existe déjà</p>}
                                      { errorNameIsExist&& <p class="text-red-500 text-xs italic">Le nom de la colonne est obligatoire</p>}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="mt-6 flex items-center justify-end gap-x-6">
                              <span
                                onClick={(e) => {
                                    if(inputColumnNameRef.current.value.length !== 0){
                                      dispatch(addColumnAndHideModal(inputColumnNameRef.current.value));
                                    }else{
                                      dispatch(setErrorName())
                                    }
                                  }
                                }
                                className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                              >
                                Enregistrer
                              </span>
                              <span
                                className="mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 sm:mt-0 sm:w-auto"
                                // onClick={() => setOpen(false)}
                                onClick={() => {
                                  dispatch(hideModalColumn());
                                  dispatch(clearErrorColumn())
                                }}
                                ref={cancelButtonRef}
                              >
                                Annuler
                              </span>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition.Root>
      )
}

export default ModalColumn;
