import Tag from "./Tag";
import { useDispatch } from 'react-redux';
import { showModalCard } from '../features/modalCardSlice';
import { setCurrentItem } from '../features/cardSlice';

const Card = ({dataCard}) => {

  const {id, column, data: {name, type, description}} = dataCard;
  const typeAction = 'update';

  const dispatch = useDispatch();

  return (
    <div className="Card w-full bg-gray-200 rounded-xl pt-2 h-32 hover:cursor-pointer" 
        onClick={() => { 
            dispatch(showModalCard(typeAction)); 
            dispatch(setCurrentItem({
              id: id,
              column: column, 
              data: {
                name: name,
                type: type,
                description: description
              }}
          ));
        }}>
        <div className="w-full p-4 bg-white rounded-xl justify-between h-full">
            <div className="flex flex-col border-b-2 pb-2">
              <Tag type={type}/>
              <h1 className="text-lg">{name}</h1>
            </div>
            <div className="mt-3">
              <p>{description}</p>
            </div>
        </div>
    </div>
  );
}

export default Card;
