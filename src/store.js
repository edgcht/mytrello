import { configureStore } from '@reduxjs/toolkit'
import columnReducer from './features/columnSlice'
import modalColumnReducer from './features/modalColumnSlice'
import modalCardReducer from './features/modalCardSlice'
import cardSliceReducer from './features/cardSlice'

export const store = configureStore({
  reducer: {
    column: columnReducer,
    modalColumn: modalColumnReducer,
    modalCard: modalCardReducer,
    card: cardSliceReducer
  },
})