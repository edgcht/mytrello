import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  modalColumnValue: false,
}

export const modalColumnSlice = createSlice({
  name: 'modalColumnSlice',
  initialState,
  reducers: {
    showModalColumn: (state) => {
      state.modalColumnValue = true
    },
    hideModalColumn: (state) => {
      state.modalColumnValue = false
    }
  },
})

// Action creators are generated for each case reducer function
export const { showModalColumn, hideModalColumn } = modalColumnSlice.actions

export default modalColumnSlice.reducer