import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  cardValue: [],
  column: '',
  currentItem: '',
  submissionCardIsValid: {
    name: true,
    type: true
  },
}

export const cardSlice = createSlice({
  name: 'card',
  initialState,
  reducers: {
    addCard: (state, action) => {
      const newCard = action.payload;

      newCard.data.name.length === 0 ? state.submissionCardIsValid.name = false : state.submissionCardIsValid.name = true;
      newCard.data.type.length === 0 ? state.submissionCardIsValid.type = false : state.submissionCardIsValid.type = true; 

      if(state.submissionCardIsValid.type && state.submissionCardIsValid.name){
        state.cardValue.push(newCard);
      }
    },
    deleteCard: (state, action) => {
      const deleteObject = action.payload;
      state.cardValue = state.cardValue.filter(card => card.id !== deleteObject.id);
    },
    updateCard: (state, action) => {
      const updateCard = action.payload[0];
      const oldObject = action.payload[1];

      updateCard.data.name.length === 0 ? state.submissionCardIsValid.name = false : state.submissionCardIsValid.name = true;
      updateCard.data.type.length === 0 ? state.submissionCardIsValid.type = false : state.submissionCardIsValid.type = true; 

      if(state.submissionCardIsValid.type && state.submissionCardIsValid.name){
        const index = state.cardValue.findIndex((element) => element.id === oldObject.id);
        if (index !== -1) {
          state.cardValue[index] = updateCard;
        }
      }
    },
    clearErrorCard: (state) => {
      state.submissionCardIsValid.name = true;
      state.submissionCardIsValid.type = true;
    },
    deleteCardByColumn: (state, action) => {
      const deleteCardByColumn = action.payload;
      state.cardValue = state.cardValue.filter(card => JSON.parse(JSON.stringify(card)).column !== deleteCardByColumn);
    },
    setCurrentItem: (state, action) => {
      state.currentItem = action.payload;
    },
    setColumn: (state, action) => {
        state.column = action.payload
    },
    getDataCard: (state) => {
      state.column.addSuccessCard = state;
    }
  },
})

// Action creators are generated for each case reducer function
export const { addCard, deleteCard, setColumn, updateCard, setCurrentItem, deleteCardByColumn, getDataCard, clearErrorCard } = cardSlice.actions

export default cardSlice.reducer