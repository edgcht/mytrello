import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  columnValue: [
    {
        name: 'To do'
    },
    {
        name: 'In Progress'
    },
    {
        name: 'Done'
    },
  ],
  errorColumnIsExist: false,
  errorNameIsExist: false,
  addSuccessColumn: false
}

export const columnSlice = createSlice({
  name: 'column',
  initialState,
  reducers: {
    addColumn: (state, action) => {
      let filter = state.columnValue.filter(column => column.name.toLocaleLowerCase() === action.payload.toLocaleLowerCase());
      // si aucun nom de colonne renseigné
      if(action.payload.length === 0){
        state.errorNameIsExist = true;
        state.addSuccessColumn = false;
      }
      else if(filter.length > 0){
        state.errorColumnIsExist = true;
        state.addSuccessColumn = false;
      }else{
        state.columnValue.push({name: action.payload});
        state.errorColumnIsExist = false;
        state.errorNameIsExist = false;
        state.addSuccessColumn = true;
      }
    },
    deleteColumn: (state, action) => {
      state.columnValue = state.columnValue.filter(column => column.name !== action.payload)
    },
    setError: (state) => {
      state.errorColumnIsExist = !state.errorColumnIsExist
    },
    setErrorName: (state) => {
      state.errorNameIsExist = !state.errorNameIsExist
    },
    clearErrorColumn: (state) => {
      state.errorColumnIsExist = false;
      state.errorNameIsExist = false;
    },
    setAddSuccessColumn: (state) => {
      state.addSuccessColumn = !state.addSuccessColumn
    },
    getDataColumn: (state) => {
      state.column.addSuccessColumn = state;
    }
  },
})

// Action creators are generated for each case reducer function
export const { addColumn, deleteColumn, setError, setErrorName, setAddSuccessColumn, getDataColumn, clearErrorColumn } = columnSlice.actions

export default columnSlice.reducer