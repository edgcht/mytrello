import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  modalCardValue: false,
  actionType: null
}

export const modalCardSlice = createSlice({
  name: 'modalCardSlice',
  initialState,
  reducers: {
    showModalCard: (state, actions) => {
      state.actionType = actions.payload
      state.modalCardValue = true
    },
    hideModalCard: (state, actions) => {
      state.actionType = actions.payload
      state.modalCardValue = false
    }
  },
})

// Action creators are generated for each case reducer function
export const { showModalCard, hideModalCard } = modalCardSlice.actions

export default modalCardSlice.reducer